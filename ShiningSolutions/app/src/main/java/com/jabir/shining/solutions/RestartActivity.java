package com.jabir.shining.solutions;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

import com.jabir.shining.solutions.activity.IntroActivity;
import com.jabir.shining.solutions.base.CommonActivity;
import com.jabir.shining.solutions.commons.Commons;
import com.jabir.shining.solutions.commons.Constants;

/**
 * Created by HugeRain on 4/30/2017.
 */

public class RestartActivity extends CommonActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!Commons.g_isAppRunning){

            String room = getIntent().getStringExtra(Constants.KEY_ROOM);

            Intent goIntro = new Intent(this, IntroActivity.class);

            if (room != null)
                goIntro.putExtra(Constants.KEY_ROOM, room);

            startActivity(goIntro);
        }

        finish();

    }


    @Override
    protected void onDestroy(){

        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){

        return true;
    }
}
