package com.jabir.shining.solutions.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jabir.shining.solutions.R;
import com.jabir.shining.solutions.base.CommonActivity;
import com.jabir.shining.solutions.commons.Constants;
import com.jabir.shining.solutions.utils.BitmapUtils;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.io.File;
import java.io.InputStream;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class AddProductActivity extends CommonActivity implements View.OnClickListener {

    EditText edt_category_id, edt_category_name, edt_product_id, edt_product_name, edt_quality, edt_alert_me;
    ImageView imv_back, imv_save, imv_product_scan, imv_category_scan, imv_photo, imv_camera;

    private Uri _imageCaptureUri;
    String _photoPath = "";

    String[] strings = null;
    MaterialSpinner spinner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        loadLayout();
    }

    private void loadLayout() {

        imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.BARCODE_PRO = "";
                Constants.BARCODE_CAT = "";

                startActivity(new Intent(AddProductActivity.this, CountryListActivity.class));
                finish();

            }
        });

        imv_save = (ImageView)findViewById(R.id.imv_save);
        imv_save.setOnClickListener(this);

        imv_product_scan = (ImageView)findViewById(R.id.imv_product_scan);
        imv_product_scan.setOnClickListener(this);

        imv_category_scan = (ImageView)findViewById(R.id.imv_category_scan);
        imv_category_scan.setOnClickListener(this);

        imv_photo = (ImageView)findViewById(R.id.imv_photo);
        imv_photo.setOnClickListener(this);

        imv_camera = (ImageView)findViewById(R.id.imv_camera);
        imv_camera.setVisibility(View.VISIBLE);

        edt_category_id = (EditText)findViewById(R.id.edt_category_id);
        edt_category_name = (EditText)findViewById(R.id.edt_category_name);
        edt_product_id =(EditText)findViewById(R.id.edt_product_id);
        edt_product_name = (EditText)findViewById(R.id.edt_product_name);
        edt_quality = (EditText)findViewById(R.id.edt_quality);
        edt_alert_me = (EditText)findViewById(R.id.edt_alert_me);

        if (Constants.BARCODE_CAT.length() > 0 /*&& Constants.KEY_TYPE == 5*/) {
            edt_category_id.setText(Constants.BARCODE_CAT);
        }

        if (Constants.BARCODE_PRO.length() > 0 /*&& Constants.KEY_TYPE == 10*/) {
            edt_product_id.setText(Constants.BARCODE_PRO);
        }



        spinner = (MaterialSpinner) findViewById(R.id.spinner);
        strings = new String[]{"Kg","G","MG"};
        spinner.setItems(strings);
        //spinner.setItems("Kg","G","MG");
        final String[] producttype = {strings[0]};
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                producttype[0] = item;

                Log.d("item===", item);

                //0: all: 1: request: 2:accepted:3:rejected

        /*        if (item.equals("All")){


                } else if (item.equals("Requested")){


                } else if (item.equals("Accepted")) {


                } else if (item.equals("Rejected")) {

                }*/
            }
        });


        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.activity_add_product);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_category_id.getWindowToken(), 0);

                return false;
            }
        });
    }

    public void selectPhoto() {

        final String[] items = {"Take photo", "Choose from Gallery","Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();

                } else if(item == 1){
                    doTakeGallery();

                } else {
                    return;
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        //_imageCaptureUri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        imv_photo.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                        if (_photoPath.length() > 0){

                            imv_camera.setVisibility(View.GONE);
                        } else imv_camera.setVisibility(View.VISIBLE);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void gotoScanActivity(){

        Intent intent = new Intent(this, ScanActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }

    private boolean checkValid(){

        if (edt_category_id.getText().toString().length() == 0){
            showAlertDialog("Please input the category ID");
            return false;

        } else if (edt_category_name.getText().toString().length() == 0){

            showAlertDialog("Please input the category Name");
            return false;

        }else if (edt_product_name.getText().toString().length() == 0){

            showAlertDialog("Please input the product Name");
            return false;

        }else if (edt_product_id.getText().toString().length() == 0){

            showAlertDialog("Please input the product ID");
            return false;
        }

        return true;
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_save:
                if (checkValid())
                showToast("Save");
                break;

            case R.id.imv_category_scan:
                Constants.KEY_TYPE = 5;
                gotoScanActivity();
                break;

            case R.id.imv_product_scan:
                Constants.KEY_TYPE = 10;
                gotoScanActivity();
                break;

            case R.id.imv_photo:
                selectPhoto();
                break;
        }

    }
}
