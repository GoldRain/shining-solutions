package com.jabir.shining.solutions.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.jabir.shining.solutions.R;
import com.jabir.shining.solutions.adapter.CountryListViewAdapter;
import com.jabir.shining.solutions.base.CommonActivity;
import com.jabir.shining.solutions.commons.Constants;
import com.jabir.shining.solutions.model.CountryEntity;

import java.util.ArrayList;

public class CountryListActivity extends CommonActivity {

    ListView lst_country_list;

    CountryListViewAdapter _adapter;
    ArrayList<CountryEntity> _countries = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_list);

        loadLayout();
    }

    private void loadLayout() {

        for (int i = 0; i < Constants.COUNTRY_NAME.length; i++){
            CountryEntity countryEntity = new CountryEntity();

            countryEntity.set_id(i);
            countryEntity.set_country_name(Constants.COUNTRY_NAME[i]);
            countryEntity.set_country_flag(Constants.FLAG[i]);

            _countries.add(countryEntity);

        }

        lst_country_list = (ListView)findViewById(R.id.lst_country_list);
        _adapter = new CountryListViewAdapter(this, _countries);
        lst_country_list.setAdapter(_adapter);

    }

    public void gotoMapActivity(String country){

        showToast(country);
        Intent intent = new Intent(this, CountryMapsActivity.class);
        intent.putExtra(Constants.KEY_LOCATION, country);
        startActivity(intent);
    }

    public void gotoAddProduct(){

        startActivity(new Intent(CountryListActivity.this, AddProductActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {

    }
}
