package com.jabir.shining.solutions.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.jabir.shining.solutions.R;
import com.jabir.shining.solutions.base.CommonActivity;

public class ForgotActivity extends CommonActivity {

    EditText edt_email;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        loadLayout();
    }

    private void loadLayout() {

        edt_email = (EditText)findViewById(R.id.edt_email) ;

        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.activity_forgot);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_email.getWindowToken(),0);
                return false;
            }
        });

    }

    public boolean checkValid(){

        if (edt_email.getText().toString().length() == 0) {

            showAlertDialog("Please input your email.");

            return false;

        } else if (!Patterns.EMAIL_ADDRESS.matcher(edt_email.getText().toString()).matches()){

            edt_email.setError("Please check your email.");

            return false;
        }

        return true;
    }

    public void sendEmail(View view){

        if (checkValid()) {

            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edt_email.getWindowToken(), 0);

            showToast("Sent Email");
        }
    }

}
