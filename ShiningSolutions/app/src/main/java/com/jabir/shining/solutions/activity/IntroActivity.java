package com.jabir.shining.solutions.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jabir.shining.solutions.R;
import com.jabir.shining.solutions.commons.Constants;

public class IntroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        gotoSignin();
    }

    private void gotoSignin() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(IntroActivity.this, SignInActivity.class));

                overridePendingTransition(0,0);
                finish();
            }
        }, Constants.SPLASH_TIME);
    }
}
