package com.jabir.shining.solutions.activity;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jabir.shining.solutions.R;
import com.jabir.shining.solutions.base.CommonActivity;
import com.jabir.shining.solutions.commons.Commons;
import com.jabir.shining.solutions.fragment.HomeFragment;
import com.jabir.shining.solutions.fragment.SettingFragment;
import com.jabir.shining.solutions.utils.RadiusImageView;

import static com.jabir.shining.solutions.R.id.fly_container;

public class MainActivity extends CommonActivity implements View.OnClickListener {

    DrawerLayout ui_drawerlayout;
    ImageView ui_imv_call_draw;
    TextView txv_title;
    LinearLayout lyt_from_pic_nav, lyt_ratting_us_nav, lyt_call_us_nav, lyt_share_us_nav;   //nav

    RadiusImageView ui_imvPhoto;
    TextView txv_name;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvPhoto = (RadiusImageView)findViewById(R.id.imv_photo_h);
        txv_name = (TextView)findViewById(R.id.txv_name);

        Glide.with(this).load("http://35.165.175.25/uploadfiles/news/2017/04/9_news_14917084151.jpg");
        txv_name.setText("Developer");

        txv_title = (TextView)findViewById(R.id.txv_title);

        ui_drawerlayout = (DrawerLayout)findViewById(R.id.drawerlayout);

        ui_imv_call_draw = (ImageView)findViewById(R.id.imv_call_drawer);
        ui_imv_call_draw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDrawer();
            }
        });


        /*NAVIGATION*/
        lyt_from_pic_nav = (LinearLayout)findViewById(R.id.lyt_from_pic_nav);
        lyt_from_pic_nav.setOnClickListener(this);

        lyt_ratting_us_nav = (LinearLayout)findViewById(R.id.lyt_ratting_us_nav);
        lyt_ratting_us_nav.setOnClickListener(this);

        lyt_call_us_nav = (LinearLayout)findViewById(R.id.lyt_call_us_nav);
        lyt_call_us_nav.setOnClickListener(this);

        lyt_share_us_nav = (LinearLayout)findViewById(R.id.lyt_share_us_nav);
        lyt_share_us_nav.setOnClickListener(this);

        gotoHomeFragment();
        ui_drawerlayout.closeDrawers();

    }


    public void showDrawer() {

        ui_drawerlayout =(DrawerLayout)findViewById(R.id.drawerlayout);

        ui_drawerlayout.openDrawer(Gravity.LEFT);
    }

    public void gotoHomeFragment(){

        txv_title.setText("Home");

        HomeFragment fragment = new HomeFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(fly_container, fragment).commit();

    }

    public void gotoSettingFragment(){

        txv_title.setText("Settings");

        SettingFragment fragment = new SettingFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(fly_container, fragment).commit();
    }


    public void gotoItem1Fragment(){

        txv_title.setText("Item1");

    }


    public void gotoItem2Fragment(){

        txv_title.setText("Item2");

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.lyt_from_pic_nav:
                gotoHomeFragment();
                ui_drawerlayout.closeDrawers();
                break;

            case R.id.lyt_ratting_us_nav:
                gotoItem1Fragment();
                ui_drawerlayout.closeDrawers();
                break;

            case R.id.lyt_call_us_nav:
                gotoItem2Fragment();
                ui_drawerlayout.closeDrawers();
                break;

            case R.id.lyt_share_us_nav:
                gotoSettingFragment();
                ui_drawerlayout.closeDrawers();
                break;
        }

    }

    @Override
    public void onBackPressed() {

        if (ui_drawerlayout.isDrawerOpen(GravityCompat.START)){

            ui_drawerlayout.closeDrawer(GravityCompat.START);

        } else onExit();
    }
}
