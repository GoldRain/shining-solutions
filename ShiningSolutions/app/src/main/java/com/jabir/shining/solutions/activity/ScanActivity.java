package com.jabir.shining.solutions.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;
import com.jabir.shining.solutions.R;
import com.jabir.shining.solutions.commons.Constants;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;
    private LinearLayout llScan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        mScannerView = new ZXingScannerView(this);

        llScan = (LinearLayout) findViewById(R.id.llScan);
        llScan.addView(mScannerView);

    }

    @Override
    public void handleResult(Result result) {

        mScannerView.stopCamera();
        if (Constants.KEY_TYPE == 5){

            Constants.BARCODE_CAT = result.getText();
        } else if (Constants.KEY_TYPE == 10){

            Constants.BARCODE_PRO = result.getText();
        }

        Intent intent = new Intent(this, AddProductActivity.class);
        intent.putExtra(Constants.KEY_BARCODE_SCAN, result.getText() );
        Log.d("Barcode==>", result.getText());
        startActivity(intent);

        // edtBarcode.setText(result.getText());
        finish();

    }

    @Override
    public void onResume() {
        super.onResume();

        // Register ourselves as a handler for scan results.
        mScannerView.setResultHandler(this);

        // Start camera on resume
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();

        // Stop camera on pause
        mScannerView.stopCamera();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constants.BARCODE_CAT="";
        Constants.BARCODE_PRO="";
        Intent intent = new Intent(this, AddProductActivity.class);
        intent.putExtra(Constants.KEY_BARCODE_SCAN, "" );
        startActivity(intent);
        // edtBarcode.setText(result.getText());
        finish();
    }

}
