package com.jabir.shining.solutions.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jabir.shining.solutions.R;
import com.jabir.shining.solutions.base.CommonActivity;
import com.jabir.shining.solutions.commons.Constants;
import com.jabir.shining.solutions.preference.PrefConst;
import com.jabir.shining.solutions.preference.Preference;

public class SignInActivity extends CommonActivity implements View.OnClickListener{

    EditText edt_email, edt_password;
    TextView txv_signin, txv_email, txv_pwd, txv_forgot;

    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,  android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_SMS,
            android.Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION };

    int MY_PEQUEST_CODE = 107;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        checkAllPermission();
        initValue();
        loadLayout();
    }

    private void initValue() {

        Intent intent = getIntent();

        try {
            _isFromLogout = intent.getBooleanExtra(Constants.KEY_LOGOUT, false);
        } catch (Exception e){}
    }

    boolean _isFromLogout = false;

    private void loadLayout() {

        Typeface custom_style = Typeface.createFromAsset(getResources().getAssets(),"fonts/papyrus.ttf");

        edt_email = (EditText)findViewById(R.id.edt_email);
        edt_password = (EditText)findViewById(R.id.edt_password);

        txv_signin = (TextView)findViewById(R.id.txv_signin);
        txv_signin.setOnClickListener(this);

        txv_email = (TextView)findViewById(R.id.txv_email);
        txv_pwd = (TextView)findViewById(R.id.txv_pwd);
        txv_forgot = (TextView)findViewById(R.id.txv_forgot);

        txv_signin.setTypeface(custom_style);
        txv_email.setTypeface(custom_style);
        txv_pwd.setTypeface(custom_style);
        txv_forgot.setTypeface(custom_style);


        edt_email.setTypeface(custom_style);
        edt_password.setTypeface(custom_style);

        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.activity_sign_in);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_email.getWindowToken(),0);
                return false;
            }
        });

        if (_isFromLogout){

            //save user to empty
            Preference.getInstance().put(this, PrefConst.PREFKEY_USEREMAIL, "");
            Preference.getInstance().put(this, PrefConst.PREFKEY_USERPWD, "");

            edt_email.setText("");
            edt_password.setText("");

        } else {

            String _name = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USEREMAIL, "");
            String _password = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USERPWD,"");

            edt_email.setText(_name);
            edt_password.setText(_password);

            if (_name.length() > 0 && _password.length() > 0)
                processLogin();
        }

    }

    private void processLogin() {

        //String url = ReqConst.SERVER_URL + ReqConst.REQ_SINGUP;
    }

    public boolean checkValid(){

        if (edt_email.getText().toString().length() == 0){

            showAlertDialog("Please input your email.");

            return false;

        } else if (edt_password.getText().toString().length() == 0){

            showAlertDialog("Please input your password.");

            return false;

        } else if (!Patterns.EMAIL_ADDRESS.matcher(edt_email.getText().toString()).matches()){

            edt_email.setError("Please check your email.");

            return false;
        }

        return true;
    }

    public void gotoForgotActivity(View view){

        startActivity(new Intent(this, ForgotActivity.class));

    }

    public void gotoMainActivity(){

        if (checkValid()){

            startActivity(new Intent(this, CountryListActivity.class));

            overridePendingTransition(0,0);
            finish();
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.txv_signin:
                gotoMainActivity();
                break;
        }
    }

    /*==================== Permission========================================*/
    public void checkAllPermission() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }

    /*//////////////////////////////////////////////////////////////////////////////*/

    public boolean hasPermissions(Context context, String... permissions) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {

                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PEQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        }

    }

}
