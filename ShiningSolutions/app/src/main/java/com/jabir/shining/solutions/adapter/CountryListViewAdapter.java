package com.jabir.shining.solutions.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jabir.shining.solutions.R;
import com.jabir.shining.solutions.activity.CountryListActivity;
import com.jabir.shining.solutions.commons.Constants;
import com.jabir.shining.solutions.model.CountryEntity;
import com.jabir.shining.solutions.utils.RadiusImageView;

import java.util.ArrayList;

/**
 * Created by HugeRain on 5/23/2017.
 */

public class CountryListViewAdapter extends BaseAdapter {


    CountryListActivity _activity;
    ArrayList<CountryEntity> _allCountry = new ArrayList<>();

    public CountryListViewAdapter(CountryListActivity activity, ArrayList<CountryEntity> countryEntities){

        this._activity = activity;
        this._allCountry = countryEntities;

    }

    @Override
    public int getCount() {
        return _allCountry.size();
    }

    @Override
    public Object getItem(int position) {
        return _allCountry.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        CountryHolder holder;
        if (convertView ==  null){

            holder = new CountryHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_country, parent, false);

            holder.imv_flag = (RadiusImageView)convertView.findViewById(R.id.imv_flag);
            holder.txv_country_name = (TextView)convertView.findViewById(R.id.txv_country_name);

            convertView.setTag(holder);

        } else {

            holder = (CountryHolder)convertView.getTag();
        }

        final CountryEntity country = (CountryEntity)_allCountry.get(position);

        Glide.with(_activity).load(country.get_country_flag()).placeholder(R.drawable.please_select).into(holder.imv_flag);
        holder.txv_country_name.setText(country.get_country_name());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  _activity.showToast(country.get_country_name());
                _activity.gotoMapActivity(country.get_country_name());*/

                _activity.gotoAddProduct();

            }
        });
        return convertView;
    }

    public class CountryHolder{

        RadiusImageView imv_flag;
        TextView txv_country_name;
    }
}
