package com.jabir.shining.solutions.commons;

import com.jabir.shining.solutions.R;

import java.util.ArrayList;

/**
 * Created by HGS on 12/11/2015.
 */
public class Constants {

    public static final int VOLLEY_TIME_OUT = 60000;
    public static final int SPLASH_TIME = 2000;
    public static final int PROFILE_IMAGE_SIZE = 256;

    public static final int RECENT_MESSAGE_COUNT = 20;


    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;

    public static final String USER = "user";
    public static final String CLASS = "class";
    public static final String KEY_ROOM = "room";

    public static final String XMPP_START = "xmpp";
    public static final int XMPP_FROMBROADCAST = 0;
    public static final int XMPP_FROMLOGIN = 1;

    public static final String KEY_SEPERATOR = "#";
    public static final String KEY_ROOM_MARKER = "ROOM#";

    public static final String KEY_LOGOUT = "logout";

    public static final int NORMAL_NOTI_ID = 1;

    public static final String KEY_IMAGEPATH = "image_path";
    public static final String KEY_POSITION = "position";

    public static final String KEY_LOCATION = "location";
    public static final String KEY_BARCODE_SCAN = "barcode_scan";
    public static int KEY_TYPE = 0;

    public static String BARCODE_CAT = "";
    public static String BARCODE_PRO = "";



    public static String[] COUNTRY_NAME = {"Abkhazia", "Afghanistan", "Aland Islands", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Argentina",
            "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Azores Island", "Bahamas", "Bahrain", "Balearic Islands", "Bangladesh", "Barbados", "Basque Country",
            "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bonaire", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Columbia", "British Indian Ocean Territory",
            "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Canary Islands", "Cape Verde", "Cayman Islands",    "Central African Republic",
            "Ceuta", "Chad", "Chile", "China", "Christmas Island", "Cocos Islands", "Colombia",
            "Comoros", "Cook Island", "Corsica", "Costa Rica", "Croatia", "Cuba", "Curacao", "Cyprus", "Czech Republic", "Democratic Republic Of Congo", "Denmark", "Djibouti",
            "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "England", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "European Union", "Falkland Islands",
            "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "Gabon", "Galapagos Islands", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam",
            "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Hawaii", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland",
            "Isle Of Man", "Israel", "Italy", "Ivory Coast", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia",
            "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Madagascar", "Madeira", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Island",
            "Martinique", "Mauritania", "Mauritius", "Melilla", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nato",
            "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Marianas Islands", "North Korea", "Norway", "Oman", "Orkney Islands", "Ossetia",
            "Pakistan", "Palau", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Portugal", "Puerto Rico", "Qatar", "Rapa Nui",
            "Republic Of Macedonia", "Republic Of Poland", "Republic Of The Congo", "Romania", "Russia", "Rwanda", "Saba Island", "Saint Kitts and Nevis", "Salvador", "Samoa", "San Marino",
            "Sao Tome and Principe", "Sardinia", "Saudi Arabia", "Scotland", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Sint Eustatius", "Sint Maarten", "Slovakia", "Slovenia",
            "Solomon Islands", "Somalia", "South Africa", "South Korea", "South Sudan", "Spain", "Sri Lanka", "St Barts", "St Lucia", "St Vincent And The Grenadines", "Sudan", "Suriname", "Swaziland",
            "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tibet", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan",
            "Turks and Caicos", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United Nations", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City",
            "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Western Sahara", "Yemen", "Zambia", "Zimbabwe"
    };



    public static int[] FLAG = {
            R.drawable.abkhazia, R.drawable.afghanistan, R.drawable.aland_islands, R.drawable.albania, R.drawable.algeria, R.drawable.american_samoa, R.drawable.andorra, R.drawable.angola, R.drawable.anguilla,
            R.drawable.argentina, R.drawable.armenia, R.drawable.aruba, R.drawable.australia, R.drawable.austria, R.drawable.azerbaijan, R.drawable.azores_islands, R.drawable.bahamas, R.drawable.bahrain,
            R.drawable.balearic_islands, R.drawable.bangladesh, R.drawable.barbados, R.drawable.basque_country, R.drawable.belarus, R.drawable.belgium, R.drawable.belize, R.drawable.benin, R.drawable.bermuda,
            R.drawable.bhutan, R.drawable.bolivia, R.drawable.bonaire, R.drawable.bosnia_and_herzegovina, R.drawable.botswana, R.drawable.brazil, R.drawable.british_columbia, R.drawable.british_indian_ocean_territory,
            R.drawable.british_virgin_islands, R.drawable.brunei, R.drawable.bulgaria, R.drawable.burkina_faso, R.drawable.burundi, R.drawable.cambodia, R.drawable.cameroon,
            R.drawable.canada, R.drawable.canary_islands, R.drawable.cape_verde, R.drawable.cayman_islands, R.drawable.central_african_republic, R.drawable.ceuta,
            R.drawable.chad, R.drawable.chile, R.drawable.china, R.drawable.christmas_island, R.drawable.cocos_island, R.drawable.colombia, R.drawable.comoros, R.drawable.cook_islands, R.drawable.corsica,
            R.drawable.costa_rica, R.drawable.croatia, R.drawable.cuba, R.drawable.curacao, R.drawable.cyprus, R.drawable.czech_republic, R.drawable.democratic_republic_of_congo, R.drawable.denmark,
            R.drawable.djibouti, R.drawable.dominica, R.drawable.dominican_republic, R.drawable.east_timor, R.drawable.ecuador, R.drawable.egypt, R.drawable.england, R.drawable.equatorial_guinea,
            R.drawable.eritrea, R.drawable.estonia, R.drawable.ethiopia, R.drawable.european_union, R.drawable.falkland_islands, R.drawable.faroe_islands, R.drawable.fiji, R.drawable.finland,
            R.drawable.france, R.drawable.french_polynesia, R.drawable.gabon, R.drawable.galapagos_islands, R.drawable.gambia, R.drawable.georgia, R.drawable.germany, R.drawable.ghana,
            R.drawable.gibraltar, R.drawable.greece, R.drawable.greenland, R.drawable.grenada, R.drawable.guam, R.drawable.guatemala, R.drawable.guernsey, R.drawable.guinea,
            R.drawable.guinea_bissau, R.drawable.guyana, R.drawable.haiti, R.drawable.hawaii, R.drawable.honduras, R.drawable.hong_kong, R.drawable.hungary, R.drawable.iceland,
            R.drawable.india, R.drawable.indonesia, R.drawable.iran, R.drawable.iraq, R.drawable.ireland, R.drawable.isle_of_man, R.drawable.israel, R.drawable.italy, R.drawable.ivory_coast,
            R.drawable.jamaica, R.drawable.japan, R.drawable.jersey, R.drawable.jordan, R.drawable.kazakhstan, R.drawable.kenya, R.drawable.kiribati, R.drawable.kosovo, R.drawable.kuwait,
            R.drawable.kyrgyzstan, R.drawable.laos, R.drawable.latvia, R.drawable.lebanon, R.drawable.lesotho, R.drawable.liberia, R.drawable.libya, R.drawable.liechtenstein, R.drawable.lithuania,
            R.drawable.luxembourg, R.drawable.macao, R.drawable.madagascar, R.drawable.madeira, R.drawable.malawi, R.drawable.malaysia, R.drawable.maldives, R.drawable.mali, R.drawable.malta,
            R.drawable.marshall_island, R.drawable.martinique, R.drawable.mauritania, R.drawable.mauritius, R.drawable.melilla, R.drawable.mexico, R.drawable.micronesia, R.drawable.moldova,
            R.drawable.monaco, R.drawable.mongolia, R.drawable.montenegro, R.drawable.montserrat, R.drawable.morocco,
            R.drawable.mozambique, R.drawable.myanmar, R.drawable.namibia, R.drawable.nato, R.drawable.nauru, R.drawable.nepal, R.drawable.netherlands, R.drawable.new_zealand,
            R.drawable.nicaragua, R.drawable.niger, R.drawable.nigeria, R.drawable.niue, R.drawable.norfolk_island, R.drawable.northern_marianas_islands, R.drawable.north_korea,
            R.drawable.norway, R.drawable.oman, R.drawable.orkney_islands, R.drawable.ossetia, R.drawable.pakistan, R.drawable.palau, R.drawable.palestine, R.drawable.panama,
            R.drawable.papua_new_guinea, R.drawable.paraguay, R.drawable.peru, R.drawable.philippines, R.drawable.pitcairn_islands, R.drawable.portugal, R.drawable.puerto_rico, R.drawable.qatar,
            R.drawable.rapa_nui, R.drawable.republic_of_macedonia, R.drawable.republic_of_poland, R.drawable.republic_of_the_congo, R.drawable.romania, R.drawable.russia, R.drawable.rwanda,
            R.drawable.saba_island, R.drawable.saint_kitts_and_nevis, R.drawable.salvador, R.drawable.samoa, R.drawable.san_marino, R.drawable.sao_tome_and_principe, R.drawable.sardinia,
            R.drawable.saudi_arabia, R.drawable.scotland, R.drawable.senegal, R.drawable.serbia, R.drawable.seychelles, R.drawable.sierra_leone, R.drawable.singapore,
            R.drawable.sint_eustatius, R.drawable.sint_maarten, R.drawable.slovakia, R.drawable.slovenia, R.drawable.solomon_islands, R.drawable.somalia, R.drawable.south_africa,
            R.drawable.south_korea, R.drawable.south_sudan, R.drawable.spain, R.drawable.sri_lanka, R.drawable.st_barts, R.drawable.st_lucia, R.drawable.st_vincent_and_the_grenadines,
            R.drawable.sudan, R.drawable.suriname, R.drawable.swaziland, R.drawable.sweden, R.drawable.switzerland, R.drawable.syria, R.drawable.taiwan, R.drawable.tajikistan,
            R.drawable.tanzania, R.drawable.thailand, R.drawable.tibet, R.drawable.togo, R.drawable.tokelau, R.drawable.tonga, R.drawable.trinidad_and_tobago, R.drawable.tunisia,
            R.drawable.turkey, R.drawable.turkmenistan, R.drawable.turks_and_caicos, R.drawable.tuvalu, R.drawable.uganda, R.drawable.ukraine, R.drawable.united_arab_emirates,
            R.drawable.united_kingdom, R.drawable.united_nations, R.drawable.united_states, R.drawable.uruguay, R.drawable.uzbekistn, R.drawable.vanuatu, R.drawable.vatican_city,
            R.drawable.venezuela, R.drawable.vietnam, R.drawable.virgin_islands, R.drawable.wales, R.drawable.western_sahara, R.drawable.yemen, R.drawable.zambia, R.drawable.zimbabwe

    };

}
