package com.jabir.shining.solutions.commons;


import com.jabir.shining.solutions.base.CommonActivity;

import java.util.logging.Handler;

/**
 * Created by HGS on 12/11/2015.
 */
public class ReqConst {

    public static final String SERVER_ADDR = "http://52.34.222.14";
    public static boolean g_isAppRunning = false;
    public static boolean g_isAppPaused = false;

    public static Handler g_handler = null;
    public static String g_appVersion = "1.0";
    public static int g_badgCount = 0;

    //public static UserEntity g_newUser = null;
    //public static UserEntity g_user = null;

    public static CommonActivity g_currentActivity = null;

    public static String idxToAddr(int idx) {
        return idx + "@" + ReqConst.CHATTING_SERVER;
    }

    public static int addrToIdx(String addr) {
        int pos = addr.indexOf("@");
        return Integer.valueOf(addr.substring(0, pos)).intValue();
    }

    public static String fileExtFromUrl(String url) {

        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }

        if (url.lastIndexOf(".") == -1) {
            return url;
        } else {
            String ext = url.substring(url.lastIndexOf(".") );
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();
        }
    }

    public static String fileNameWithExtFromUrl(String url) {

        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }

        if (url.lastIndexOf("/") == -1) {
            return url;
        } else {

            String name = url.substring(url.lastIndexOf("/")  + 1);
            return name;
        }
    }

    public static String fileNameWithoutExtFromUrl(String url) {

        String fullname = fileNameWithExtFromUrl(url);

        if (fullname.lastIndexOf(".") == -1) {
            return fullname;
        } else {
            return fullname.substring(0, fullname.lastIndexOf("."));
        }
    }

    public static String fileNameWithExtFromPath(String path) {

        if (path.lastIndexOf("/") > -1)
            return path.substring(path.lastIndexOf("/") + 1);

        return path;
    }

    public static String fileNameWithoutExtFromPath(String path) {

        String fullname = fileNameWithExtFromPath(path);

        if (fullname.lastIndexOf(".") == -1) {
            return fullname;
        } else {
            return fullname.substring(0, fullname.lastIndexOf("."));
        }
    }
    public static final String CHATTING_SERVER = "52.34.222.14";

//    public static final String SERVER_ADDR = "http://192.168.0.40/CampusGlue";
//    public static final String CHATTING_SERVER = "192.168.0.40";

    public static final String ROOM_SERVICE = "@conference.";

    public static final String SERVER_URL = SERVER_ADDR + "/index.php/api/";

    //Request value
    public static final String REQ_SINGUP = "signup";


    //response value
    public static final String RES_CODE = "result_code";


    public static final int CODE_SUCCESS = 0;


}
