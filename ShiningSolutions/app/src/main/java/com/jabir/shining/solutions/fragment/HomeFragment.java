package com.jabir.shining.solutions.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jabir.shining.solutions.R;
import com.jabir.shining.solutions.activity.MainActivity;


@SuppressLint("ValidFragment")
public class HomeFragment extends Fragment {

    MainActivity _activity;
    View view;

    public HomeFragment(MainActivity activity) {
        // Required empty public constructor

        _activity = activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);

        loadLayout();

        return view;
    }

    private void loadLayout() {

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity =(MainActivity)context;
    }
}
