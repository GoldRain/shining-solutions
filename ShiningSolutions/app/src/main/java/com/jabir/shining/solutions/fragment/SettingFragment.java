package com.jabir.shining.solutions.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jabir.shining.solutions.R;
import com.jabir.shining.solutions.activity.MainActivity;


@SuppressLint("ValidFragment")
public class SettingFragment extends Fragment implements View.OnClickListener{

    MainActivity _activity;
    View view;

    EditText edt_password, edt_confirm;
    Button btn_confirm;

    public SettingFragment(MainActivity activity) {
        // Required empty public constructor

        this._activity = activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_setting, container, false);

        loadLayout();
        return view;
    }

    private void loadLayout() {

        edt_password = (EditText)view.findViewById(R.id.edt_password);
        edt_confirm = (EditText)view.findViewById(R.id.edt_confirm);

        btn_confirm = (Button)view.findViewById(R.id.btn_confirm);
        btn_confirm.setOnClickListener(this) ;

        LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.lyt_container);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager)_activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_password.getWindowToken(), 0);
                return false;
            }
        });

    }

    public boolean isValid(){

        if(edt_password.getText().length() == 0){
            _activity.showAlertDialog(getString(R.string.inputPassword));
            return false;

        }/*else if(edt_password.getText().length()<6){
            _activity.showAlertDialog(getString(R.string.PwdLength));
            return false;
        }*/else if(edt_confirm.getText().length() == 0){

            _activity.showAlertDialog(getString(R.string.inputPwdConfirm));

            return false;

        }else if(!edt_password.getText().toString().equals(edt_confirm.getText().toString())){
            _activity.showAlertDialog(getString(R.string.checkPwd));
            return false;
        }
        return true;
    }

    public void changePassword(){

        _activity.showToast("Changed your password");
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btn_confirm:

                if (isValid()){

                    InputMethodManager imm = (InputMethodManager)_activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt_password.getWindowToken(), 0);

                    changePassword();
                }
                break;
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

}
