package com.jabir.shining.solutions.model;

import com.jabir.shining.solutions.R;

/**
 * Created by HugeRain on 5/23/2017.
 */

public class CountryEntity {

    int _id = 0;
    String _country_name = "";
    Integer _country_flag = R.drawable.please_select;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_country_name() {
        return _country_name;
    }

    public void set_country_name(String _country_name) {
        this._country_name = _country_name;
    }

    public Integer get_country_flag() {
        return _country_flag;
    }

    public void set_country_flag(Integer _country_flag) {
        this._country_flag = _country_flag;
    }
}
