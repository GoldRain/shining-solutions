package com.jabir.shining.solutions.preference;

/**
 * Created by HugeRain on 4/30/2017.
 */

public class PrefConst {

    public static final String PREFKEY_USEREMAIL = "email";
    public static final String PREFKEY_XMPPID = "XMPPID";
    public static final String PREFKEY_USERPWD = "password";

    public static final String PREFKEY_LASTLOGINID = "lastlogin_id";

}
